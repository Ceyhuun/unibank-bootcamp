package az.ingress.microservicebootcamp.controller;

public class Airplane implements Test2{
    @Override
    public boolean canFly() {
        return true;
    }

    @Override
    public String drive() {
        return "Plane is driving";
    }
}
