package az.ingress.microservicebootcamp;

import az.ingress.microservicebootcamp.controller.SampleController;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
@EnableConfigurationProperties
public class MicroserviceBootcampApplication implements CommandLineRunner {

    private final SampleController controller;

    public static void main(String[] args) {
        SpringApplication.run(MicroserviceBootcampApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        String s = controller.sayHello("1");
        log.info(s);
    }
}
